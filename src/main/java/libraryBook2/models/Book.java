package libraryBook2.models;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @Column(name = "book_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int bookId;

    @Column(name = "title")
    @NotEmpty(message = "Нужно ввести название!")
    @Size(min = 2, max = 50, message = "Название должно быть в диапазоне от 2 до 50 символов")
    private String title;

    @Column(name = "year_of_publishing")
    @Min(value = 0, message = "Год должен быть валидным!")
    private int yearOfPublishing;

    @Column(name = "author")
    @NotEmpty(message = "Нужно ввести автора!")
    @Size(min = 2, max = 50, message = "Имя автора должно быть в диапазоне от 2 до 50 символов")
    private String author;

    @ManyToOne
    @JoinColumn(name = "person_id", referencedColumnName = "person_id")
    private Person holder;

    @Column(name = "date_took_book")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTookBook;

    @Transient
    private boolean overdueBook;

    public Book() {
    }

    public Book(String title, int yearOfPublishing, String author) {
        this.title = title;
        this.yearOfPublishing = yearOfPublishing;
        this.author = author;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Person getHolder() {
        return holder;
    }

    public void setHolder(Person holder) {
        this.holder = holder;
    }

    public Date getDateTookBook() {
        return dateTookBook;
    }

    public void setDateTookBook(Date dateTookBook) {
        this.dateTookBook = dateTookBook;
    }

    public boolean isOverdueBook() {
        return overdueBook;
    }

    public void setOverdueBook(boolean overdueBook) {
        this.overdueBook = overdueBook;
    }
}
