package libraryBook2.conroller;

import libraryBook2.models.Book;
import libraryBook2.models.Person;
import libraryBook2.services.BookService;
import libraryBook2.services.PersonService;
import libraryBook2.util.BookValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/library")
public class LibraryController {
    private final PersonService personService;
    private final BookService bookService;
    private final BookValidator bookValidator;

    @Autowired
    public LibraryController(PersonService personService, BookService bookService, BookValidator bookValidator) {
        this.personService = personService;
        this.bookService = bookService;
        this.bookValidator = bookValidator;
    }

    @GetMapping
    public String showListBook(@RequestParam(value = "page", required = false) Integer page,
                               @RequestParam(value = "books_per_page", required = false) Integer booksPerPage,
                               @RequestParam(value = "sort_by_year", required = false) Boolean sortByYear, Model model) {
        if (page == null) page = -1;
        if (booksPerPage == null) booksPerPage = 0;
        if (sortByYear == null) sortByYear = false;
        if (sortByYear) {
            if (page >= 0 && booksPerPage > 0) model.addAttribute("library",
                    bookService.showListBookSortYear(page, booksPerPage));
            else model.addAttribute("library", bookService.showListBookSortYear());
        } else {
            if (page >= 0 && booksPerPage > 0)
                model.addAttribute("library", bookService.showListBook(page, booksPerPage));
            else model.addAttribute("library", bookService.showListBook());
        }

        return "library/showListBook";
    }

    @GetMapping("/{book_id}")
    public String showBook(@PathVariable("book_id") int bookId, Model model, @ModelAttribute("personAdd") Person person) {

        model.addAttribute("book", bookService.showBook(bookId));

        model.addAttribute("people", personService.showListPeople());

        model.addAttribute("person", personService.showPersonWhoTookBook(bookId));

        return "library/showBook";
    }
    
    @GetMapping("/createNewBook")
    public String createNewBook(@ModelAttribute("book") Book book) {

        return "library/createNewBook";
    }

    @PostMapping
    public String createBook(@ModelAttribute("book") @Valid Book book, BindingResult bindingResult) {
        bookValidator.validate(book, bindingResult);
        if (bindingResult.hasErrors()) return "library/createNewBook";

        bookService.createNewBook(book);

        return "redirect:/library";
    }

    @GetMapping("/{book_id}/editBook")
    public String editBookGet(@PathVariable("book_id") int bookId, Model model) {
        model.addAttribute("book", bookService.showBook(bookId));

        return "library/editBook";
    }

    @PatchMapping("/{book_id}")
    public String editBookPatch(@ModelAttribute("book") @Valid Book book, BindingResult bindingResult,
                                @PathVariable("book_id") int bookId) {

        bookValidator.validate(book, bindingResult);
        if (bindingResult.hasErrors()) return "library/editBook";

        bookService.editBook(bookId, book);

        return "redirect:/library";
    }

    @DeleteMapping("/{book_id}")
    public String deleteBook(@PathVariable("book_id") int bookId) {
        bookService.deleteBook(bookId);
        return "redirect:/library";
    }

    @PatchMapping("/{book_id}/release")
    public String releaseBook(@PathVariable("book_id") int bookId) {
        bookService.releaseBook(bookId);
        return "redirect:/library/{book_id}";
    }

    @PatchMapping("/{book_id}/addPersonBook")
    public String addPersonBook(@PathVariable("book_id") int bookId, @ModelAttribute("personAdd") Person person) {

        bookService.addPersonBook(bookId, person);

        return "redirect:/library/{book_id}";
    }

    @GetMapping("/search")
    public String searchBookForName() {

        return "library/searchBook";
    }

    @PostMapping("/search")
    public String searchBookForNamePatch(@RequestParam("searchBookForName") String searchBookForName, Model model) {

        model.addAttribute("listFindBooks", bookService.findByTitleStartingWith(searchBookForName));

        return "library/searchBook";
    }
}

















