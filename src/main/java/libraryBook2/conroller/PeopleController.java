package libraryBook2.conroller;

import libraryBook2.models.Person;
import libraryBook2.services.BookService;
import libraryBook2.services.PersonService;
import libraryBook2.util.PersonValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/people")
public class PeopleController {

    private final BookService bookService;
    private final PersonService personService;
    private final PersonValidator personValidator;

    @Autowired
    public PeopleController(BookService bookService, PersonService personService, PersonValidator personValidator) {
        this.bookService = bookService;
        this.personService = personService;
        this.personValidator = personValidator;
    }

    @GetMapping
    public String showListPeople(Model model) {
        model.addAttribute("people", personService.showListPeople());

        return "people/showListPeople";
    }

    @GetMapping("/{person_id}")
    public String showPerson(@PathVariable("person_id") int personId, Model model) {
        model.addAttribute("person", personService.showPerson(personId));

        model.addAttribute("check", bookService.findByHolder(personId));

        return "people/showPerson";
    }

    @GetMapping("/createNewPerson")
    public String createNewPerson(@ModelAttribute("person") Person person) {

        return "people/createNewPerson";
    }

    @PostMapping
    public String createPerson(@ModelAttribute("person") @Valid Person person, BindingResult bindingResult) {
        personValidator.validate(person, bindingResult);
        if (bindingResult.hasErrors()) return "people/createNewPerson";

        personService.createNewPerson(person);

        return "redirect:/people";
    }

    @GetMapping("/{person_id}/editPerson")
    public String editPersonGet(@PathVariable("person_id") int personId, Model model) {
        model.addAttribute("person", personService.showPerson(personId));

        return "people/editPerson";
    }

    @PatchMapping("/{person_id}")
    public String editPersonPatch(@ModelAttribute("person") @Valid Person person, BindingResult bindingResult,
                                  @PathVariable("person_id") int personId) {

        personValidator.validate(person, bindingResult);
        if (bindingResult.hasErrors()) return "people/editPerson";

        personService.editPerson(personId, person);

        return "redirect:/people";
    }

    @DeleteMapping("/{person_id}")
    public String deletePerson(@PathVariable("person_id") int personId) {
        personService.deletePerson(personId);
        return "redirect:/people";
    }

}
