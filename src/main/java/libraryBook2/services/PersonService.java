package libraryBook2.services;

import libraryBook2.models.Person;
import libraryBook2.repositories.BookRepository;
import libraryBook2.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class PersonService {
    private final PersonRepository personRepository;
    private final BookRepository bookRepository;

    @Autowired
    public PersonService(PersonRepository personRepository, BookRepository bookRepository) {
        this.personRepository = personRepository;
        this.bookRepository = bookRepository;
    }

    public List<Person> showListPeople() {
        return personRepository.findAll();
    }

    public Person showPerson(int personId) {
        Optional<Person> optionalPerson = personRepository.findById(personId);
        return optionalPerson.orElse(null);
    }

    public Optional<Person> showPerson(String name) {
        return personRepository.findByName(name);

    }

    @Transactional
    public void createNewPerson(Person person) {
        personRepository.save(person);
    }

    @Transactional
    public void editPerson(int personId, Person editPerson) {
        editPerson.setPersonId(personId);
        personRepository.save(editPerson);
    }

    @Transactional
    public void deletePerson(int personId) {
        personRepository.deleteById(personId);
    }

    public Person showPersonWhoTookBook(int bookId) {
        return bookRepository.findById(bookId).get().getHolder();
    }
}
