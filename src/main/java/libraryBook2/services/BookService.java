package libraryBook2.services;

import libraryBook2.models.Book;
import libraryBook2.models.Person;
import libraryBook2.repositories.BookRepository;
import libraryBook2.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class BookService {
    private static final long DAY_FOR_DEAD_LINE = 24 * 60 * 60 * 1000;
    private final BookRepository bookRepository;
    private final PersonRepository personRepository;

    @Autowired
    public BookService(BookRepository bookRepository, PersonRepository personRepository) {
        this.bookRepository = bookRepository;
        this.personRepository = personRepository;
    }

    public List<Book> showListBook() {
        return checkForOverdue(bookRepository.findAll());
    }

    public List<Book> showListBook(int page, int booksPerPage) {
        return checkForOverdue(bookRepository.findAll(PageRequest.of(page, booksPerPage)).getContent());
    }

    public List<Book> showListBookSortYear() {
        return checkForOverdue(bookRepository.findAll(Sort.by("yearOfPublishing")));
    }

    public List<Book> showListBookSortYear(int page, int booksPerPage) {
        return checkForOverdue(bookRepository.findAll(PageRequest.of(page, booksPerPage, Sort.by("yearOfPublishing"))).getContent());
    }

    public Book showBook(int bookId) {
        Optional<Book> optionalPerson = bookRepository.findById(bookId);
        return optionalPerson.orElse(null);
    }

    public Optional<Book> showBook(String title) {
        return bookRepository.findByTitle(title);
    }

    @Transactional
    public void createNewBook(Book book) {
        bookRepository.save(book);
    }

    @Transactional
    public void editBook(int bookId, Book editBook) {
        editBook.setBookId(bookId);
        bookRepository.save(editBook);
    }

    @Transactional
    public void deleteBook(int bookId) {
        bookRepository.deleteById(bookId);
    }

    public List<Book> counter(int personId) {
        return personRepository.findById(personId).get().getBooks();
    }

    @Transactional
    public void releaseBook(int bookId) {
        bookRepository.findById(bookId).get().setHolder(null);
    }

    @Transactional
    public void addPersonBook(int bookId, Person person) {

        bookRepository.findById(bookId).get().setHolder(person);
        bookRepository.findById(bookId).get().setDateTookBook(new Date());
    }

    public List<Book> findByHolder(int personId) {
        return bookRepository.findByHolder(personRepository.getOne(personId));
    }

    public List<Book> findByTitleStartingWith(String startingWith) {
        return bookRepository.findByTitleContainingIgnoreCase(startingWith);
    }

    private List<Book> checkForOverdue(List<Book> listBook) {
        for (int i = 0; i < listBook.size(); i++) {
            if (listBook.get(i).getDateTookBook() != null)
                if (new Date().getTime() - listBook.get(i).getDateTookBook().getTime() > 10 * DAY_FOR_DEAD_LINE)
                    listBook.get(i).setOverdueBook(true);
        }
        return listBook;
    }
}