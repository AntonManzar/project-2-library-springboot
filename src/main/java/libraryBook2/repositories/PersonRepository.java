package libraryBook2.repositories;

import libraryBook2.models.Book;
import libraryBook2.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person,Integer> {
    Optional<Person> findByName(String name);

    List<Book> findByBooks(int personId);
}
